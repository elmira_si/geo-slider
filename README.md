# README #

Plugin Allows to Add Regions and Auidences under each region

### View of plugin backend ###

**All Regions List(Geo Slider->Regions)
![1.png](https://bitbucket.org/repo/zrnBRg/images/1279647521-1.png)

1. To Add New Region click to Add New Region button above
2. To Edit/Delete Region and go to list All Audiences for that region you should hover over the Title of region and click to link as you like

Edit Screen for Region
![7.png](https://bitbucket.org/repo/zrnBRg/images/3425472954-7.png)

** All Audiences list(Geo Slider->Audiences)
![2.png](https://bitbucket.org/repo/zrnBRg/images/307100191-2.png)

1. Add New Audience Edit Form
2. Filter Audience By Region. After filter you’ll see new “Default Slider” button, its for first slider for each region

Edit screen for Audience
![6.png](https://bitbucket.org/repo/zrnBRg/images/3780010138-6.png)


![3.png](https://bitbucket.org/repo/zrnBRg/images/1635687718-3.png)

Default Slider button opens Edit Screen for it
![4.png](https://bitbucket.org/repo/zrnBRg/images/440479767-4.png)

**Set Defoult Region (Geo Slider->Settings)
![5.png](https://bitbucket.org/repo/zrnBRg/images/892055260-5.png)