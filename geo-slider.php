<?php
/**
 * Geo Slider
 *
 * @wordpress-plugin
 * Plugin Name:       Geo Slider
 * Plugin URI:        
 * Description:       Plugin allowed to show slider content by geo tag
 * Version:           1.0.0
 * Author:            EK
 * Author URI:        http://example.com/
 * Text Domain:       geo-slider
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

register_activation_hook(__FILE__, 'ls_install');

function ls_install() {

    
}

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
//require plugin_dir_path( __FILE__ ) . 'includes/class-geo-slider-front.php';
require plugin_dir_path( __FILE__ ) . 'includes/register-gs-post-type.php';
require plugin_dir_path( __FILE__ ) . 'includes/class-geo-slider-admin.php';

//allow redirection, even if my theme starts to send output to the browser
add_action('init', 'do_output_buffer');
function do_output_buffer() {
    ob_start();
}

add_action('init', 'GSStartSession', 1);
function GSStartSession() {
    if(!session_id()) {
        session_start();
    }
}


/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function rungeo_slider_result() {
    //$plugin = new Geo_Slider_Render();
    $plugin = new GS_Admin_Settings();
	
}
rungeo_slider_result();