<?php
/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package     Geo Slider
 * @subpackage  geo-slider/includes
 */

/**
 * The core plugin class.
 * 
 * @package     Geo Slider Admin Settings
 * @subpackage  geo-slider/includes
 * @author      Elmira Kirakosyan <elmirakirakosyan@gmail.com>
 */
class GS_Admin_Settings {
    
   
	
    public function __construct() { 
        
        add_action( 'admin_menu', array( $this, 'gs_admin_menu' ) );  
        
    }
    
    /**
    * Admin menu page and subpages, 
    * Callback function for action admin_menu
    */    
    function gs_admin_menu () {
        
        add_menu_page('Geo Slider Settings', 'Geo Slider', 'manage_options', 'regions', array($this,'geo_region_output'), '', 20 );
        add_submenu_page('regions', 'Geo Slider Regions', 'Regions', 'manage_options', 'regions' );
        add_submenu_page('regions', 'Geo Audience Settings', 'Audiences', 'manage_options', 'geo-audience', array($this,'gs_audience_callback') );
        add_submenu_page('regions', 'Geo Slider Settings', 'Settings', 'manage_options', 'geo-regions', array($this,'gs_settings_callback') );
        
    }
    
    
    
   
    /**
    * Regions page Callback function
    */
    function geo_region_output()
    {        
        $terms = self::getAllRegions(); 
        
        //add new region
        if( isset( $_POST['act-region'] ) ){
            
            $taxonomy = 'geo_category';
            $post_type = 'geo-slider';
            $tax = get_taxonomy( $taxonomy );
        
            if ( $_POST['action'] == 'add-region' ){
                if ( ! current_user_can( $tax->cap->edit_terms ) ) {
                    wp_die(
                        '<h1>' . __( 'Cheatin&#8217; uh?' ) . '</h1>' .
                        '<p>' . __( 'Sorry, you are not allowed to add this item.' ) . '</p>',
                        403
                    );
                }
                $ret = wp_insert_term( $_POST['tag-name'], $taxonomy, $_POST );
                $term_id = $ret['term_id']; 
                if ( $ret ){ 
                    $term_id = $ret['term_id'];
                    add_term_meta ($term_id, 'region_icon', $_POST['region_icon'], true);
                    $location = 'admin.php?page=regions';
               } 
            }
            
            if ( $_POST['action'] == 'edit-region' ){
                if ( ! current_user_can( $tax->cap->edit_terms ) ) {
                    wp_die(
                            '<h1>' . __( 'Cheatin&#8217; uh?' ) . '</h1>' .
                            '<p>' . __( 'Sorry, you are not allowed to edit this item.' ) . '</p>',
                            403
                    );
                }
                $region_ID = $_GET['edit_region_id'];
                $ret = wp_update_term( $region_ID, $taxonomy, $_POST );
                if ( $ret ){ 
                    update_term_meta( $region_ID, 'region_icon', $_POST['region_icon'] );
                    $location = 'admin.php?page=regions';
                } 
            }
            //redirect to regions list
            if ( $location ) {
                if ( ! empty( $_REQUEST['paged'] ) ) {
                    $location = add_query_arg( 'paged', (int) $_REQUEST['paged'], $location );
                }                
                wp_redirect( $location );
                exit;
            }
        }        
       
        //include proper template 
        if( $_GET['action'] == 'add-new' ){
            require_once plugin_dir_path( __FILE__ ) . 'partials/_add_new_region.php';
        }
        elseif ( $_GET['edit_region_id'] ){ 
            require_once plugin_dir_path( __FILE__ ) . 'partials/_edit_region.php';
        }else {
            require_once plugin_dir_path( __FILE__ ) . 'partials/_table_regions.php';
        }
        
        if ( $_GET['delete_region_id'] ){ 
            
            wp_delete_term( $_GET['delete_region_id'], $taxonomy );
            
            $location = 'admin.php?page=regions';
            wp_redirect( $location );
            exit;
        }
        
    }
    
    /**
    * Audience page Callback function
    */
    function gs_audience_callback()
    {
        global $wpdb, $post_type_object;
        $taxonomy = 'geo_category';
        $post_type = 'geo-slider';
        
        //Regions for select with each audience
        $regions = self::getAllRegions(); 
                
        $post_type_object = get_post_type_object( $post_type );
               
        //include proper template 
        if( $_GET['action'] == 'add-new' ){
            
            if ( isset($_POST['act-audience']) ){                
                if( $_POST['action'] == 'add-audience' ){
                    
                    if ( ! current_user_can( $post_type_object->cap->edit_posts ) || ! current_user_can( $post_type_object->cap->create_posts ) ) {
                        wp_die(
                                '<h1>' . __( 'Cheatin&#8217; uh?' ) . '</h1>' .
                                '<p>' . __( 'Sorry, you are not allowed to create posts as this user.' ) . '</p>',
                                403
                        );
                    }                    
                    // Create post object
                    $audience_args = array(
                        'post_title'    => wp_strip_all_tags( $_POST['audience-title'] ),
                        'post_content'  => $_POST['audience-description'],
                        'post_status'   => 'publish',
                        'post_type' => $post_type,
                        'post_category' => array(1),
                        'taxonomy'      => 'geo_category'
                    );
                    
                    // Insert the audience post into the database
                    $audienceInsert_ID = wp_insert_post( $audience_args );
                    if ( $audienceInsert_ID ){ 
                        wp_set_object_terms($audienceInsert_ID, $_POST['audience-region'], 'geo_category', true);
                        add_post_meta( $audienceInsert_ID, 'audience_logo', $_POST['audience-logo'], true );
                        add_post_meta( $audienceInsert_ID, 'audience_content_logo', $_POST['audience-content-logo'], true );
                        
                        $gridTitles = $_POST['grid-titles'];
                        $gridUrls = $_POST['grid-urls'];
                        $gridCounts = $_POST['grid-counts'];
                        
                        add_post_meta( $audienceInsert_ID, 'audience_grid_titles', serialize($gridTitles), true );
                        add_post_meta( $audienceInsert_ID, 'audience_grid_urls', serialize($gridUrls), true );
                        add_post_meta( $audienceInsert_ID, 'audience_grid_counts', serialize($gridCounts), true );
                        
                        $location = 'admin.php?page=geo-audience';
                    } 
                    
                }
                
                //redirect to regions list
                if ( $location ) {
                    if ( ! empty( $_REQUEST['paged'] ) ) {
                        $location = add_query_arg( 'paged', (int) $_REQUEST['paged'], $location );
                    }                
                    wp_redirect( $location );
                    exit;
                }
                
            }
            
            require_once plugin_dir_path( __FILE__ ) . 'partials/_add_new_audience.php';
        }
        elseif ( $_GET['edit_audience_id'] ){             
            if ( isset($_POST['act-audience']) ){                
                if( $_POST['action'] == 'edit-audience' ){
                    $editId = $_GET['edit_audience_id'];

                    // Update the audience post
                    
                    $audience_args = array(
                        'ID' => $editId,
                        'post_title'    => wp_strip_all_tags( $_POST['audience-title'] ),
                        'post_content'  => $_POST['audience-description'],
                        'post_status'   => 'publish',
                        'post_type' => $post_type,
                        'post_category' => array(1),
                        'taxonomy'      => 'geo_category'
                    );
                    $editAudience = wp_update_post( $audience_args, true );
                    
                    if ( $editAudience ){ 
                        
                        wp_remove_object_terms( $editId, $_POST['selected-region'], 'geo_category');
                        wp_set_object_terms( $editId, $_POST['audience-region'], 'geo_category', true);
                        update_post_meta( $editId, 'audience_logo', $_POST['audience-logo'] );
                        update_post_meta( $editId, 'audience_content_logo', $_POST['audience-content-logo'] );

                        $gridTitles = $_POST['grid-titles'];
                        $gridUrls = $_POST['grid-urls'];
                        $gridCounts = $_POST['grid-counts'];

                        update_post_meta( $editId, 'audience_grid_titles', serialize($gridTitles) );
                        update_post_meta( $editId, 'audience_grid_urls', serialize($gridUrls) );
                        update_post_meta( $editId, 'audience_grid_counts', serialize($gridCounts) );

                        $location = 'admin.php?page=geo-audience';
                    }             
                }
            }
            require_once plugin_dir_path( __FILE__ ) . 'partials/_edit_audience.php';
        }
        elseif ( $_GET['default-slider'] ){
            if ( isset($_POST['act-audience']) ){                
                if( $_POST['action'] == 'edit-default-audience' ){                    
                    
                    if( isset($_POST['post-id']) && $_POST['post-id'] != ''){
                        $editId = $_POST['post-id'];
                        $args = array(
                            'ID' => $editId,
                            'post_title'    => wp_strip_all_tags( $_POST['audience-title'] ),
                            'post_content'  => $_POST['audience-description'],
                            'post_status'   => 'publish',
                            'post_type' => 'geo-default-slider'
                        );
                        $update = wp_update_post( $args, true );
                        
                        if ( $update ){ 
                            wp_remove_object_terms( $editId, $_POST['audience-region'], 'geo_default_category');
                            wp_set_object_terms( $editId, $_POST['audience-region'], 'geo_default_category', true);
                            update_post_meta( $editId, 'audience_logo', $_POST['audience-logo'] );
                            update_post_meta( $editId, 'audience_pager_title', $_POST['audience-pager-title'] );
                            update_post_meta( $editId, 'audience_pager_subtitle', $_POST['audience-pager-subtitle'] );
                        } 
                    }else{
                        // Create post object
                        $args = array(
                            'post_title'    => wp_strip_all_tags( $_POST['audience-title'] ),
                            'post_content'  => $_POST['audience-description'],
                            'post_status'   => 'publish',
                            'post_type' => 'geo-default-slider'
                        );

                        // Insert the audience default slider post into the database
                        $insertID = wp_insert_post( $args );
                        if ( $insertID ){ 
                            wp_set_object_terms($insertID, $_POST['audience-region'], 'geo_default_category', true);
                            add_post_meta( $insertID, 'audience_logo', $_POST['audience-logo'], true );                           
                            add_post_meta( $insertID, 'audience_pager_title', $_POST['audience-pager-subtitle'], true );                           
                            add_post_meta( $insertID, 'audience_pager_subtitle', $_POST['audience-pager-subtitle'], true );                           
                        } 
                    }
                }
                //redirect to regions list
                if ( $location ) {      
                    $location = 'admin.php?page=geo-audience';
                    wp_redirect( $location );
                    exit;
                }
            }
            
            require_once plugin_dir_path( __FILE__ ) . 'partials/_default_audience.php';
        }
        else {
            require_once plugin_dir_path( __FILE__ ) . 'partials/_table_audience.php';
        }
        
        if( $_GET['delete_audience_id'] ){            
            wp_delete_post($_GET['delete_audience_id']);
            $location = 'admin.php?page=geo-audience';
            wp_redirect( $location );
            exit;
        }
    }
    
    function getAllRegions(){
        
        global $wpdb;
        $taxonomy = 'geo_category';
        $term_query = new WP_Term_Query();
        $args = array(
            'taxonomy' => $taxonomy,
            'hide_empty' => false,
            'orderby' => 'ID'
        );
        $terms = $term_query->query( $args );  
        
        return $terms;
    }


    /**
    * Settings page Callback function
    */
    function gs_settings_callback() {
        
        $regions = self::getAllRegions();
        
        if ( !get_option( 'default-region' ) ){
            add_option( 'default-region', $regions[0]->slug, '', 'yes' );
        }
        
        if ( isset( $_POST['save-settings'] ) ){
            update_option( 'default-region', $_POST['audience-region'], 'yes' );
        }
        
        $defaultRegion = get_option( 'default-region' );
       
        require_once plugin_dir_path( __FILE__ ) . 'partials/_settings.php';

    }
    
    
    
    
    
   
    
}
