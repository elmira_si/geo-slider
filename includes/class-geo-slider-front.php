<?php
/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package     Email Search Result
 * @subpackage  email-search-result/includes
 */

/**
 * The core plugin class.
 * 
 * @package     Email Search Result
 * @subpackage  email-search-result/includes
 * @author      Elmira Kirakosyan <elmirakirakosyan@gmail.com>
 */
class Geo_Slider_Render {
	
    public function __construct() {
 
        load_plugin_textdomain( 'email-search-result', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
 
        add_action( 'wp_enqueue_scripts', array( $this, 'register_ers_styles' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'register_esr_scripts' ) );
 
        add_action('wp_head', array( $this, 'append_mailbox_to_search' ) );
        
    }
    
    public function register_ers_styles() { 
        wp_register_style( 'email-search-result', plugins_url( 'email-search-result/css/styles.css' ) );
        wp_enqueue_style( 'email-search-result' );
    }

    public function register_esr_scripts() {       
        
        if ( ! wp_script_is( 'jquery', 'enqueued' )) {
            wp_register_script( 'jquery', plugins_url( 'email-search-result/js/jquery-2.1.4.min.js' ) );
            wp_enqueue_script( 'jquery' );
        }
        wp_register_script( 'email-search-result', plugins_url( 'email-search-result/js/scripts.js' ) );
        wp_enqueue_script( 'email-search-result' );
    }
    
    public function append_mailbox_to_search(  ) { 
        
        if ( is_search() ){
            $ifShow = get_option('esr_if_form_popup');
            
            if( $ifShow ){
            
                global $wpdb;
                $emailLists = $wpdb->get_results("SELECT * FROM `email_search_result_list` ");



                $dir = plugin_dir_path( __FILE__ );

                if ( isset( $_POST['email_addresses'] ) ){

                    $emailTo = $_POST['email_addresses'];

                    $emailListiD = (int) $_POST['email_list'];

                    if ( $emailListiD != "" ){
                        $emails = $wpdb->get_row("SELECT * FROM `email_search_result_list` WHERE id = " . $emailListiD );
                        $allAddressess = $emails->addresses;
                        $multiple_recipients = explode(";", $allAddressess);
                    } else {
                        $multiple_recipients = $emailTo;
                    }
                    //var_dump($multiple_recipients);die;


                    $emailFrom = get_option('esr_template_email_from');
                    $tpl_title = get_option('esr_template_title');
                    $tpl_description = get_option('esr_template_description');
                    $tpl_footer = get_option('esr_template_footer');

                    $subject = $_POST['email_subject'] ? $_POST['email_subject'] : 'Spaincoastrealty Best Offers For You';

                    //Prepare headers for HTML
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $headers .= 'From: Spaincoastrealty Best Offer <'.$emailFrom.'>' . "\r\n";

                    $searchContent = $this->search_result_content();

                    //Get e-mail template     
                    $message_template = file_get_contents($dir . 'email-templates/clasic-light.html'); 
                    //var_dump($message_template);

                    $message = str_ireplace('[search-post-loop-content]', $searchContent , $message_template); 
                    $message = str_ireplace('[email-tpl-title]', $tpl_title, $message); 
                    $message = str_ireplace('[email-tpl-description]', $tpl_description, $message); 
                    $message = str_ireplace('[email-tpl-footer]', $tpl_footer, $message); 


                    wp_mail($multiple_recipients, $subject, $message, $headers);                
                }

                echo "<div id='form_block'>";
                echo "<div class='minus'>X</div>";
                echo "<h4 class='esr_title'>Email This Search Result </h4>";
                echo "<form method='post'>";
                echo "<input type='text' name='email_subject' placeholder='Subject: Coastrealty Offers' required >";            
                if ( is_user_logged_in() ) { 
                    echo "<input type='text' name='email_addresses' placeholder='To: example@gmail.com' >";
                     echo "<select name='email_list'>";
                        echo "<option> --- Select Email List --- </option>";
                        if( !empty( $emailLists ) ){
                            foreach ($emailLists as $list ) {
                                echo "<option value='".$list->id."'>".$list->title."</option>";
                            }
                        }
                     echo "</select>";

                }else{
                    echo "<input type='text' name='email_addresses' placeholder='To: example@gmail.com' required >";
                }
                echo "<input type='submit' name='email_result' value='Send Result By Email' class='moretag btn btn-primary' >";
                echo "</form>";

                echo "</div>";
            }
        }
        
        
    }
    
     public function widgetFormContent(){
        
         if ( is_search() ){
            global $wpdb;
            $emailLists = $wpdb->get_results("SELECT * FROM `email_search_result_list` ");
            
            
            
            $dir = plugin_dir_path( __FILE__ );
            
            if ( isset( $_POST['email_addresses'] ) ){
                
                $emailTo = $_POST['email_addresses'];
                
                $emailListiD = (int) $_POST['email_list'];
             
                if ( $emailListiD != "" ){
                    $emails = $wpdb->get_row("SELECT * FROM `email_search_result_list` WHERE id = " . $emailListiD );
                    $allAddressess = $emails->addresses;
                    $multiple_recipients = explode(";", $allAddressess);
                } else {
                    $multiple_recipients = $emailTo;
                }
                //var_dump($multiple_recipients);die;
                
                
                $emailFrom = get_option('esr_template_email_from');
                $tpl_title = get_option('esr_template_title');
                $tpl_description = get_option('esr_template_description');
                $tpl_footer = get_option('esr_template_footer');
                
                $subject = $_POST['email_subject'] ? $_POST['email_subject'] : 'Spaincoastrealty Best Offers For You';
                
                //Prepare headers for HTML
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From: Spaincoastrealty Best Offer <'.$emailFrom.'>' . "\r\n";
                
                $searchContent = $this->search_result_content();
                
                //Get e-mail template     
                $message_template = file_get_contents($dir . 'email-templates/clasic-light.html'); 
                //var_dump($message_template);
                
                $message = str_ireplace('[search-post-loop-content]', $searchContent , $message_template); 
                $message = str_ireplace('[email-tpl-title]', $tpl_title, $message); 
                $message = str_ireplace('[email-tpl-description]', $tpl_description, $message); 
                $message = str_ireplace('[email-tpl-footer]', $tpl_footer, $message); 
                
               
                wp_mail($multiple_recipients, $subject, $message, $headers);
                
            }
            $cnt = "";
            $cnt .= "<div id='form_widget'>";
            $cnt .= "<form method='post'>";
            $cnt .= "<input type='text' name='email_subject' placeholder='Subject: Coastrealty Offers' required >";            
            if ( is_user_logged_in() ) { 
                $cnt .= "<input type='text' name='email_addresses' placeholder='To: example@gmail.com' >";
                 $cnt .= "<select name='email_list'>";
                    $cnt .= "<option> --- Select Email List --- </option>";
                    if( !empty( $emailLists ) ){
                        foreach ($emailLists as $list ) {
                            $cnt .= "<option value='".$list->id."'>".$list->title."</option>";
                        }
                    }
                 $cnt .= "</select>";
                 
            }else{
                $cnt .= "<input type='text' name='email_addresses' placeholder='To: example@gmail.com' required >";
            }
            $cnt .= "<input type='submit' name='email_result' value='Send Result By Email' class='moretag btn btn-primary pull-right' >";
            $cnt .= "</form>";
            
            $cnt .= "</div>";
            
        }
         
        return $cnt;
         
    }

	
    public function search_result_content(){
	

        
//        global $query_string;        
//
//        $query_args = explode("&", $query_string);
//     	$search_query = array('posts_per_page' => false, 'order' => 'ASC');
//        //$search_query = array('details_1' => '', 'details_2' => '', 'details_3' => '', 'details_4' => '', 'arg_5' => '', 'price' => '');
//
//		foreach($query_args as $key => $string) {
//                $query_split = explode("=", $string);
//                $search_query[$query_split[0]] = urldecode($query_split[1]);
//	
//		} // foreach
		
        $args = wpsight_listing_search_query_args();
        $search = new WP_Query($args);
        $content = "";
		
		//add_filter( 'pre_get_posts', 'search_result_content' );

        // The Loop
        if ( $search->have_posts() ) {
               
            while ( $search->have_posts() ) {
                $search->the_post();
                
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
                $url = $thumb['0'];
                
                $plotSize = get_post_meta ( get_the_ID(), '_details_1', true );
                $livingArea = get_post_meta ( get_the_ID(), '_details_2', true );                
                $bedrooms = get_post_meta ( get_the_ID(), '_details_3', true );
                $price = get_post_meta ( get_the_ID(), '_price', true );
               
                $homeIcon = plugins_url( 'email-search-result/images/iconHome.png');
                $livingIcon = plugins_url( 'email-search-result/images/iconArea.png');
                $bedIcon = plugins_url( 'email-search-result/images/iconBed.png');
                
                $s = substr(get_the_excerpt(), 0, 200 );
                $result = substr($s, 0, strrpos($s, ' '));

                $content .= '<table bgcolor="">
                            <tr>
                                <td class="small" width="20%" style="vertical-align:top; padding-right:10px;border-bottom:1px solid #999;">
                                <img src="'.$url.'" />
                                </td>
                                <td cellspacing="0" cellpadding="0" style="vertical-align:top;padding:0;margin:0;border-bottom:1px solid #999;" >';
                    $content .=  '<h4 style="margin:0;"><a href="' . get_permalink() . '">'. get_the_title() .'</a><td> <img src="'.$bedIcon.'" /> '.$price.'</td></h4>';
                    $content .=  '<p class="">' . $result . '... </p>';
                    $content .=  '<table bgcolor="" style="margin-top:10px;">';                    
                    $content .=  '<td> <img src="'.$homeIcon.'" /> '.$plotSize.' m<sup>2</sup></td>';                    
                    $content .=  '<td> <img src="'.$livingIcon.'" /> '.$livingArea.' m<sup>2</sup></td>';                    
                    $content .=  '<td> <img src="'.$bedIcon.'" /> '.$bedrooms.'</td>';
                    $content .=  '<tr>';
                     $content .= '</tr></table>';
               $content .=      '</td>
                            </tr>
                        </table>';
                
                
            }
               
        } else {
               $content .= "No Post Found";
        }
        /* Restore original Post Data */
        wp_reset_postdata();
        
        return $content;
        
    }
    
}
