<style type="text/css">
    .text-right{
        text-align: right;
    }
    .grid-title .button{
        margin-left: 20px;
        margin-bottom: 20px;
    }
    .grid-title .button:hover{
        color: #57c544;
    }
    .grid-title .dashicons-plus-alt{
        margin-top: 3px;
        display: inline-block;
        float: left;
        margin-right: 5px;
    }
    .grid-item{
        margin-bottom: 15px;
        border-bottom: 1px dotted #c1bbbb;
        padding-bottom: 15px;
    }
    .grid-item input.title{
        width: 35%;
        margin-right: 10px;
    }
    .grid-item input.logo{
        width: 43%;
        margin-right: 10px;
    }
    .grid-item input.count{
        width: 15%;
        margin-right: 10px;
    }
    .grid-content{
        background: #fff;
        padding: 30px 15px;
        box-sizing: border-box;
        width: 96%;
    }
    .act-audience{
        margin-top: 50px !important;
    }
    .remove-grid-item{
        background: #555555;
        border-radius: 15px;
        width: 20px;
        height: 20px;
        display: inline-block;
        color: #fff;
        cursor: pointer;
    }
    .remove-grid-item:hover{
        background: #f00;
    }
</style>

<div class="form-wrap" >
    <h2>Add New Audience</h2>
    
    <form id="addtag" method="post" action="" class="validate">
        <input type="hidden" name="action" value="add-audience">
       
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required term-name-wrap">
                    <th scope="row"><label for="audience-title">Title</label></th>
                    <td>
                        <input name="audience-title" id="post-title" type="text" value="" size="40" aria-required="true">
                        <p>The title is how it appears on your site.</p>
                    </td>
                </tr>
                <tr class="form-field term-slug-wrap">
                    <th scope="row"><label for="audience-logo">Logo URL</label></th>
                    <td>
                        <input name="audience-logo" id="audience-logo" type="url" value="" size="40">
                        <p>Audience Logo URL</p>
                    </td>
                </tr>
                <tr class="form-field term-slug-wrap"  style="border-top: 1px solid #999;">
                    <th scope="row"><label for="audience-content-logo">Content Logo URL</label></th>
                    <td>
                        <input name="audience-content-logo" id="audience-content-logo" type="url" value="" size="40">
                        <p>Audience Content Logo URL(it will show inside audience slider content)</p>
                    </td>
                </tr>
                <tr class="form-field term-description-wrap">
                    <th scope="row"><label for="audience-description">Content Short Description</label></th>
                    <td>
                        <textarea name="audience-description" id="audience-description" rows="5" cols="40"></textarea>                        
                    </td>
                </tr>
                
                <tr class="form-field term-description-wrap">
                    <th scope="row"><label for="audience-region">Select Region</label></th>
                    <td>
                        <select name="audience-region">
                            <?php foreach ($regions as $region) { ?>
                                <option value="<?php echo $region->name; ?>"><?php echo $region->name; ?></option>
                            <?php }?>
                            
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
        <hr/>
        <h4 class="grid-title">
            Audience Content Grid Items        
            <a class="button" id="clone-grid-item" ><i class="dashicons-before dashicons-plus-alt"></i>Add New</a>
        </h4>
        <div class="grid-content">
            
            <!-- <div class="grid-item">
                <input class="title" type="text" placeholder="Enter Title" />
                <input class="logo" type="url" placeholder="Enter Logo URL" />
                <input class="count" type="text" placeholder="Enter Count"/>
                <span class="remove-grid-item">
                    <i class="dashicons-before dashicons-minus"></i>
                </span>
            </div> -->
            
        </div>
        <hr/>

        <p class="submit act-audience">
            <input type="submit"name="act-audience" id="submit" class="button button-primary" value="Add Audience">
        </p>
        
    </form>
    
</div>

<script type="text/javascript">
    
    jQuery(document).ready(function($) {
	//do jQuery stuff when DOM is ready
        
        var gridContent = 
            '<div class="grid-item">\n\
                <input name="grid-titles[]" class="title" type="text" placeholder="Enter Title" />\n\
                <input name="grid-urls[]" class="logo" type="url" placeholder="Enter Logo URL" />\n\
                <input name="grid-counts[]" class="count" type="text" placeholder="Enter Count"/>\n\
                <span class="remove-grid-item"><i class="dashicons-before dashicons-minus"></i></span>\n\
            </div> ';
        //clone grid Item
        $('#clone-grid-item').on('click', function(){            
            $('.grid-content').append(gridContent);            
        });
        
        //remove-grid-item
        $( ".grid-content" ).on( "click", ".remove-grid-item", function() {
            $( this ).parent().remove();
        });
        
    });
    
</script>