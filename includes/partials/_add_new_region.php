
<div class="form-wrap" >
    <h2>Add New Region</h2>
    
    <form id="addtag" method="post" action="" class="validate">
        <input type="hidden" name="action" value="add-region">
       
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required term-name-wrap">
                    <th scope="row"><label for="name">Region Name</label></th>
                    <td>
                        <input name="tag-name" id="tag-name" type="text" value="" size="40" aria-required="true">
                        <p>The name is how it appears on your site.</p>
                    </td>
                </tr>
                <tr class="form-field term-slug-wrap">
                    <th scope="row"><label for="slug">Region Slug</label></th>
                    <td>
                        <input name="slug" id="tag-slug" type="text" value="" size="40">
                        <p>The “slug” is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.</p>
                    </td>
                </tr>
                
                <tr class="form-field term-slug-wrap">
                    <th scope="row"><label for="slug">Region Icon URL</label></th>
                    <td>
                        <input name="region_icon" id="tag-region-icon" type="url" value="" size="40">
                        <p>Region Icon for use in popup menu to select a region</p>
                    </td>
                </tr>
                
                <tr class="form-field term-description-wrap">
                    <th scope="row"><label for="description">Region Description</label></th>
                    <td>
                        <textarea name="description" id="tag-description" rows="5" cols="40"></textarea>                        
                    </td>
                </tr>
            </tbody>
        </table>

        <p class="submit">
            <input type="submit"name="act-region" id="submit" class="button button-primary" value="Add Region">
        </p>
    </form>
    
</div>