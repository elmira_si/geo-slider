<style type="text/css">
    .submit.act-audience{
        margin: 20px 0 0 0;
    }
</style>

<?php 
    global $wp;

    $args = array(
        'post_type' => 'geo-default-slider',
        'geo_default_category' => $_GET['default-slider'],
        'posts_per_page' => 1
    );


    $the_query = new WP_Query( $args );
    $title = ''; $description = ''; $imgUrl = ''; $id = '';
    $termName = $_GET['default-slider'];
   
    if ( $the_query->have_posts() ) { 
        while ( $the_query->have_posts() ) {
            $the_query->the_post(); 
            //$terms = wp_get_post_terms( get_the_ID(), 'geo_category' );
            $id = get_the_ID();
            $title = get_the_title();
            $description = get_the_content();
            $imgUrl = get_post_meta($id, 'audience_logo', true);
            $pagerTitle = get_post_meta($id, 'audience_pager_title', true);
            $pagerSubTitle = get_post_meta($id, 'audience_pager_subtitle', true);
        } 
    }
    wp_reset_postdata();            
?>

<div class="form-wrap" >
    <h2>Default Audience Slider Content</h2>
    
    <form id="addtag" method="post" action="" class="validate">
        <input type="hidden" name="action" value="edit-default-audience"/>
        <input type="hidden" name="post-id" value="<?php echo $id; ?>"/>
        <input type="hidden" name="audience-region" value="<?php echo $termName; ?>" />
        <table class="form-table">
            <tbody>
                 
                <tr class="form-field term-slug-wrap">
                    <th scope="row"><label for="audience-logo">Background Image</label></th>
                    <td>
                        <input name="audience-logo" id="audience-logo" type="url" value="<?php echo $imgUrl; ?>" size="40">
                        <p>Background Image URL</p>
                    </td>
                </tr>
                <tr class="form-field form-required term-name-wrap">
                    <th scope="row"><label for="audience-title">Title</label></th>
                    <td>
                        <input name="audience-title" id="audience-title" type="text" value="<?php echo $title; ?>" size="40" aria-required="true">
                        <p>The title is how it appears on your site.</p>
                    </td>
                </tr>
                <tr class="form-field term-description-wrap">
                    <th scope="row"><label for="audience-description">Description</label></th>
                    <td>
                        <textarea name="audience-description" id="audience-description" rows="5" cols="40"><?php echo $description; ?></textarea>                        
                    </td>
                </tr>
                
                <tr class="form-field form-required term-name-wrap" style="border-top: 1px dotted #999;">
                    <th scope="row"><label for="audience-title">Audiences Default Title</label></th>
                    <td>
                        <input name="audience-pager-title" id="audience-pager-title" type="text" value="<?php echo $pagerTitle; ?>" size="40" aria-required="true">
                    </td>
                </tr>
                <tr class="form-field form-required term-name-wrap">
                    <th scope="row"><label for="audience-title">Audiences Default SubTitle</label></th>
                    <td>
                        <input name="audience-pager-subtitle" id="audience-pager-subtitle" type="text" value="<?php echo $pagerSubTitle; ?>" size="40" aria-required="true">
                    </td>
                </tr>
            </tbody>
        </table>
        

        <p class="submit act-audience">
            <input type="submit"name="act-audience" id="submit" class="button button-primary" value="Save Default Slider">
        </p>
        
    </form>
    
</div>

