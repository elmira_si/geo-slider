<?php
$term_id = $_GET['edit_region_id'];
$taxonomy = 'geo_category';
$term = get_term( $term_id, $taxonomy );
$regionIcon = get_metadata('term', $term_id, 'region_icon', true);

?>

<div class="wrap">
    <h1>Edit Region</h1>


    <div id="ajax-response"></div>

    <form name="edittag" id="edittag" method="post" action="" class="validate">
        <input type="hidden" name="action" value="edit-region">
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required term-name-wrap">
                    <th scope="row"><label for="name">Name</label></th>
                    <td>
                        <input name="name" id="name" type="text" value="<?php echo $term->name; ?>" size="40" aria-required="true">
                        <p class="description">The name is how it appears on your site.</p>
                    </td>
                </tr>
                <tr class="form-field term-slug-wrap">
                    <th scope="row"><label for="slug">Slug</label></th>
                    <td>
                        <input name="slug" id="slug" type="text" value="<?php echo $term->slug; ?>" size="40">
                        <p class="description">The “slug” is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.</p>
                    </td>
                </tr>
                <tr class="form-field term-slug-wrap">
                    <th scope="row"><label for="slug">Region Icon URL</label></th>
                    <td>
                        <input name="region_icon" id="tag-region-icon" type="url" value="<?php echo $regionIcon; ?>" size="40">
                        <p>Region Icon for use in popup menu to select a region</p>
                    </td>
                </tr>                
                <tr class="form-field term-description-wrap">
                    <th scope="row"><label for="description">Description</label></th>
                    <td>
                        <textarea name="description" id="description" rows="5" cols="50" class="large-text"><?php echo $term->description; ?></textarea>
                    </td>
                </tr>
            </tbody>
        </table>
        <p class="submit">
            <input type="submit" name="act-region" id="submit" class="button button-primary" value="Update Region"></p>
    </form>
</div>