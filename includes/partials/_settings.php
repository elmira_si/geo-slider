<div class="wrap">
    <h1 style="margin-bottom: 20px;">Settings </h1>

    
    <form id="addtag" method="post" action="" class="validate">
       
        <hr/>
        <table class="form-table">
            <tbody>
                <tr class="form-field term-description-wrap">
                    <th scope="row"><label for="audience-region">Select Default Region</label></th>
                    <td>                       
                        <select name="audience-region">
                            <?php foreach ( $regions as $region ) { ?>
                                <option value="<?php echo $region->slug; ?>" <?php if($defaultRegion == $region->slug ) echo 'selected="selected"' ?> ><?php echo $region->name; ?></option>
                            <?php }?>                            
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
        
        <hr/>
        <p class="submit act-audience">
            <input type="submit" name="save-settings" id="submit" class="button button-primary" value="Save">
        </p>
        
    </form>
    
</div>