<?php 
global $wp;

$args = array(
        'post_type' => 'geo-slider',
        //'order' => 'ASC',
        //'orderby' => 'ID',        
);

if( $_GET['region'] && $_GET['region'] != '' ){
    $args['geo_category'] = $_GET['region'];
}

$the_query = new WP_Query( $args );
?>

<div class="wrap">
    <h1 style="margin-bottom: 20px;">
        Audiences 
        <a href="admin.php?page=geo-audience&action=add-new" class="page-title-action">Add New Audience</a>
        <?php if( $_GET['region'] && $_GET['region'] != '' ){ ?>
            <a href="admin.php?page=geo-audience&default-slider=<?php echo $_GET['region']; ?>" class="page-title-action">Default Slider</a>
        <?php }?>        
    </h1>

    <div class="tablenav top">

    <div class="alignleft actions bulkactions" style="margin-bottom: 20px;">
        <form method="GET" >
            <input type="hidden" name="page" value="geo-audience"/>
            <select name="region" id="bulk-action-selector-top">
                <option value="">Select Region</option>
                <?php foreach ($regions as $term) {?>                
                    <option <?php if($term->slug == $_GET['region'] ) { echo 'selected="selected" ';}?> value="<?php echo $term->slug; ?>"><?php echo $term->name; ?></option>
                <?php } ?>
            </select>
            <input type="submit" id="doaction" class="button action" value="Filter">
        </form>
    </div>

        </div>

    <table class="widefat fixed" cellspacing="0" >
        <thead>
        <tr>
            <!--<th id="cb" class="manage-column column-cb check-column" scope="col">ID</th>--> 
            <!--<th id="columnname" class="manage-column column-columnname" scope="col" style="width:35px;">ID</th>-->
            <th id="columnname" class="manage-column column-columnname" scope="col">Title</th>
            <th id="columnname" class="manage-column column-columnname" scope="col">Region</th> 
        </tr>
        </thead>
        <tbody>
            <?php 
                if ( $the_query->have_posts() ) { 
                    while ( $the_query->have_posts() ) {
                    $the_query->the_post(); 
                    $terms = wp_get_post_terms( get_the_ID(), 'geo_category' );
            ?>    
                <tr class="alternate" valign="top"> 
                <!--<th class="column-columnname" ><?php echo get_the_ID(); ?></th>-->
                <td class="column-columnname"><?php the_title() ; ?>
                    <div class="row-actions">
                        <span><a href="admin.php?page=geo-audience&edit_audience_id=<?php  echo get_the_ID(); ?>" >Edit</a> |</span>
                        <span><a href="admin.php?page=geo-audience&delete_audience_id=<?php  echo get_the_ID(); ?>" >Delete</a></span>                       
                    </div>
                </td>
                <td class="column-columnname"><?php echo $terms[0]->name; ?></td>
                </tr>
                <?php 
                    } 
                        }
                        wp_reset_postdata();
                ?>
                
            
        </tbody>
    </table>

</div>