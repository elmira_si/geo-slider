
<style type="text/css">
    .reg-img{
        width: 28px;
        height: 28px;
        border-radius: 20px;
        border: 1px solid #0079B1;
    }
</style>

<div class="wrap">
    <h1 style="margin-bottom: 20px;">Regions <a href="admin.php?page=regions&action=add-new" class="page-title-action">Add New Region</a></h1>

    <table class="widefat fixed" cellspacing="0" >
        <thead>
        <tr>
            <!--<th id="cb" class="manage-column column-cb check-column" scope="col">ID</th>--> 
            <!--<th id="columnname" class="manage-column column-columnname" scope="col" style="width:35px;">ID</th>-->
            <th id="columnname" class="manage-column column-columnname" scope="col" style="width:80px;" >Icon</th>
            <th id="columnname" class="manage-column column-columnname" scope="col">Title</th>
            <th id="columnname" class="manage-column column-columnname" scope="col">Slug</th> 
            <!--<th id="columnname" class="manage-column column-columnname" scope="col">Audience Count</th>--> 
        </tr>
        </thead>
        <tbody>
            <?php foreach ($terms as $term) {?>
            <?php $regionIcon = get_metadata('term',  $term->term_id, 'region_icon', true); ?>
         
            <tr class="alternate" valign="top"> 
                <!--<th class="column-columnname" ><?php echo $term->term_id; ?></th>-->
                <td class="column-columnname">
                    <?php if($regionIcon){ ?>
                        <img src="<?php echo $regionIcon; ?>" class="reg-img" />
                    <?php } ?>
                </td>
                <td class="column-columnname"><?php echo $term->name; ?>
                    <div class="row-actions">
                        <span><a href="admin.php?page=regions&edit_region_id=<?php echo $term->term_id;?>">Edit</a> |</span>
                        <span><a href="admin.php?page=regions&delete_region_id=<?php echo $term->term_id;?>">Delete</a> |</span>
                        <span><a href="admin.php?page=geo-audience&region=<?php echo $term->slug;?>" >All Audiences</a></span>                        
                    </div>
                </td>
                <td class="column-columnname"><?php echo $term->slug; ?></td>
                <!--<td class="column-columnname">6</td>-->
            </tr>
            <?php } ?>
        </tbody>
    </table>

</div>