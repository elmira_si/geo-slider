<?php

function register_geo_slider_custom_post() {
        $args = array(
          'public' => true,
          'label'  => 'Geo Slider Custom Posts',
           'show_in_menu'=> false
        );
        register_post_type( 'geo-slider', $args );
}
add_action( 'init', 'register_geo_slider_custom_post' );

// function: products_category BEGIN
function geo_category()
{
    register_taxonomy(
            __( "geo_category" ),
            array(__( "geo-slider" )),
            array(
                    "hierarchical" => true,
                    "label" => __( "Category" ),
                    "singular_label" => __( "Category" ),
                    'show_ui'           => true,
                    'show_admin_column' => true,
                    "rewrite" => array(
                            'slug' => 'geo_category',
                            'hierarchical' => true
                    )
            )
    );
}
add_action( 'init', 'geo_category', 0 );

function register_geo_default_slider_custom_post() {
        $args = array(
            'public' => true,
            'label'  => 'Geo Default Slider Custom Posts',
            'show_in_menu'=> false
        );
        register_post_type( 'geo-default-slider', $args );
}
add_action( 'init', 'register_geo_default_slider_custom_post' );

// function: products_category BEGIN
function geo_default_slider_category()
{
    register_taxonomy(
            __( "geo_default_category" ),
            array(__( "geo-default-slider" )),
            array(
                    "hierarchical" => true,
                    "label" => __( "Category" ),
                    "singular_label" => __( "Category" ),
                    'show_ui'           => true,
                    'show_admin_column' => true,
                    "rewrite" => array(
                            'slug' => 'geo_default_category',
                            'hierarchical' => true
                    )
            )
    );
}
add_action( 'init', 'geo_default_slider_category', 0 );